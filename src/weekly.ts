import fetch from "node-fetch";
import Trade from "./trade.model";
import dotenv from "dotenv";

dotenv.config();
const FREQTRADE_URL = process.env.FREQTRADE_URL;
const FREQTRADE_AUTHORIZATION = process.env.FREQTRADE_AUTHORIZATION;
if (!FREQTRADE_AUTHORIZATION || !FREQTRADE_URL)
  throw new Error(
    "Specify environment variables FREQTRADE_URL and FREQTRADE_AUTHORIZATION."
  );
const API = `${FREQTRADE_URL}/api/v1`;

const bearer = async () => {
  const response = await fetch(`${API}/token/login`, {
    headers: {
      accept: "application/json, text/plain, */*",
      authorization: `Basic ${FREQTRADE_AUTHORIZATION}`,
      "content-type": "application/json;charset=UTF-8",
    },
    body: "{}",
    method: "POST",
  });
  const json: {
    access_token: string;
    refresh_token: string;
  } = await response.json();
  return json.access_token;
};

const getTrades = async ({
  limit,
  offset,
}: {
  limit: number;
  offset: number;
}): Promise<{
  total_trades: number;
  trades: Trade[];
  trades_count: number;
}> => {
  const response = await fetch(
    `${API}/trades?limit=${limit}&offset=${offset}`,
    {
      headers: {
        accept: "application/json, text/plain, */*",
        authorization: `Bearer ${await bearer()}`,
      },
      method: "GET",
    }
  );
  return response.json();
};

const main = async () => {
  const { total_trades } = await getTrades({ limit: 1, offset: 0 });
  const limit = 7 * 7;
  const { trades } = await getTrades({
    limit,
    offset: total_trades - limit,
  });
  let i = 0;
  const weekly = [];
  while (i < trades.length) {
    const week = trades.slice(i, i + 7);
    const profitPercent = Number(
      week
        .reduce((previous, current) => previous + current.profit_pct, 0)
        .toFixed(2)
    );
    const profitAbsolute = Number(
      week
        .reduce((previous, current) => previous + current.profit_abs, 0)
        .toFixed(2)
    );
    weekly.push({
      begin: week[0].close_date,
      end: week[week.length - 1].close_date,
      profitPercent,
      profitAbsolute,
      strategy: week[0].strategy,
    });
    i += 7;
  }
  console.table(weekly.reverse());
};

main();
