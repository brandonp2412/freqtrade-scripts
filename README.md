# Weekly

This script combines trades from a [Freqtrade](https://github.com/freqtrade/freqtrade) bot in groups of seven, and
reports the profit from these trades.

# Prerequisites

Two [environment variables](https://wiki.archlinux.org/title/Environment_variables) are required to be set before this script can work.

```
FREQTRADE_URL="https://my-freqtrade-url.com"
FREQTRADE_AUTHORIZATION="AUTHORIZATION_TOKEN_FROM_API"
```

These can be set via the command line, or by creating a `.env` file.

# Building

```sh
npx tsc
```

# Running 

![](images/weekly-output.png)
